module recentdl.main;

import recentdl.rest;

import vibe.core.core : runApplication;
import vibe.core.log;
import vibe.http.fileserver : serveStaticFiles;
import vibe.http.router : URLRouter;
import vibe.http.server : HTTPServerSettings, listenHTTP;
import vibe.web.web : registerWebInterface;

import std.conv : to;
import std.exception : enforce;
import std.process : environment;

int main(string[] args)
{
	enforce("API_KEY" in environment, "Env var API_KEY is not set");
	enforce("LISTEN_ADDRESS" in environment, "Env var LISTEN_ADDRESS is not set");
	enforce("LISTEN_PORT" in environment, "Env var LISTEN_ADDRESS is not set");

	auto router = new URLRouter;
	router.registerWebInterface(new RestService);
	router.get("*", serveStaticFiles("public/"));

	auto settings = new HTTPServerSettings;
	settings.bindAddresses = [environment["LISTEN_ADDRESS"]];
	settings.port = environment["LISTEN_PORT"].to!ushort;

	listenHTTP(settings, router);

	return runApplication(&args);
}
