module recentdl.media.fetcher;

import recentdl.media.filename_parser;
import recentdl.media.media;
import recentdl.media.mediainfo;

import vibe.core.log;
import vibe.data.json : Json, parseJsonString;
import vibe.http.client : requestHTTP;
import vibe.textfilter.urlencode : urlEncode;

import std.algorithm : sort;
import std.exception : enforce;
import std.format : format;

MediaElement[] fetch(string apiKey) @trusted
{
  import std.net.curl : byLine;
  import std.string : split;

  MediaElement[] mediaElements;
  mediaElements ~= fetchHistory(apiKey);
  mediaElements ~= fetchMagnets(apiKey);

  mediaElements.sort!((a, b) => a.date > b.date);

  return mediaElements;
}

MediaElement[] fetchHistory(string apiKey)
{
  MediaElement[] mediaElements;

  Json history = requestHTTP(
    "https://api.alldebrid.com/v4/user/history?agent=recent-dl&apikey=" ~ apiKey,
    (scope req) {},
  ).readJson;
  enforce(history["status"].get!string == "success");

  foreach (file; history["data"]["links"])
  {
    if (file["host"].get!string == "error" || file["size"].get!long < 1_000_000)
      continue;

    MediaElement me;

    MediaFile mf;
    mf.url = file["link_dl"].get!string;
    mf.fileName = file["filename"].get!string;
    mf.size = file["size"].to!long;
    mf.date = file["date"].to!long;

    try
    {
      ParserResult parserResult = parseFilename(mf.fileName);
      me.title = parserResult.title;
      string suffix = " ";
      if (parserResult.season != -1)
        suffix ~= format!"S%d"(parserResult.season);
      if (parserResult.episode != -1)
        suffix ~= format!"E%d"(parserResult.episode);
      me.title ~= suffix;
    }
    catch (Exception e)
    {
      logWarn("Filename parser exception: %s", e);
    }

    me.files ~= mf;
    me.date = mf.date;

    try
    {
      MediaInfoResult mediaInfoResult = mediaInfo(me.files[0].url, me.files[0].size);
      me.audioLangs = mediaInfoResult.audioLangs;
      me.subLangs = mediaInfoResult.subLangs;
    }
    catch (Exception e)
    {
      logWarn("Mediainfo exception: %s", e);
    }

    if (me.title.length == 0)
      me.title = me.files[0].fileName;

    mediaElements ~= me;
  }

  return mediaElements;
}

MediaElement[] fetchMagnets(string apiKey)
{
  MediaElement[] mediaElements;

  Json magnets = requestHTTP(
    "https://api.alldebrid.com/v4/magnet/status?agent=recent-dl&apikey=" ~ apiKey,
    (scope req) {},
  ).readJson;
  enforce(magnets["status"].get!string == "success");

  foreach (magnet; magnets["data"]["magnets"])
  {
    if (magnet["statusCode"] != 4 || magnet["links"].length < 1)
      continue;

    MediaElement me;

    me.date = magnet["uploadDate"].get!long;

    ParserResult parserResult = parseFilename(magnet["filename"].get!string);
    if (parserResult.title.length != 0)
      me.title = parserResult.title;

    int season = parserResult.season;

    long linkCount = magnet["links"].length;
    string folderName = magnet["filename"].get!string;
    foreach (file; magnet["links"])
    {
      MediaFile mf;
      mf.fileName = file["filename"].get!string;

      if (linkCount == 1)
        mf.url = format!"https://myfiles.alldebrid.com/%s/magnets/%s"(
          apiKey, (mf.fileName)
        );
      else if (linkCount > 1)
        mf.url = format!"https://myfiles.alldebrid.com/%s/magnets/%s/%s"(
          apiKey, (folderName), (mf.fileName)
        );

      mf.size = file["size"].get!long;
      mf.date = me.date;

      // logInfo(mf.fileName);

      if (mf.size < 1_000_000)
        continue;

      try
      {
        ParserResult fileParserResult = parseFilename(mf.fileName);
        if (fileParserResult.episode != -1)
          mf.episode = fileParserResult.episode;
        if (me.title.length == 0) // Sometimes title and season aren't in the magnet name, so we try the individual files
          me.title = fileParserResult.title;
        if (season == -1)
          season = fileParserResult.season;
      }
      catch (Exception e)
      {
        logWarn("Filename parser exception: %s", e);
      }

      me.files ~= mf;
    }

    if (season != -1)
    {
      if (me.title.length != 0)
        me.title ~= format!" S%d"(season);
      else
        me.title = format!"Season %d"(season);
    }

    if (linkCount == 1 && me.files[0].episode != -1)
    {
      me.title ~= format!"E%d"(me.files[0].episode);
    }

    try
    {
      MediaInfoResult mediaInfoResult = mediaInfo(me.files[0].url, me.files[0].size);
      me.audioLangs = mediaInfoResult.audioLangs;
      me.subLangs = mediaInfoResult.subLangs;
    }
    catch (Exception e)
    {
      logWarn("Mediainfo exception: %s", e);
    }

    if (me.title.length == 0)
      me.title = me.files[0].fileName;

    mediaElements ~= me;
  }

  return mediaElements;
}
