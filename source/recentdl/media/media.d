module recentdl.media.media;

struct MediaFile
{
  string url;
  string fileName;

  long size;
  long date;

  int episode = -1;
}

/** 
 * files.length == 1 => Single file
 * files.length > 1 => Pack 
 */
struct MediaElement
{
  MediaFile[] files;
  string title;
  long date;
  string[] audioLangs;
  string[] subLangs;
}