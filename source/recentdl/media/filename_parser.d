module recentdl.media.filename_parser;

import vibe.data.json;

import std.process : ProcessPipes, pipeProcess, wait, Config, Redirect;
import std.stdio : readln, writeln;
import std.string : strip;

enum PythonCode = "
import json
import PTN
while True:
  print(json.dumps(PTN.parse(input())))
";

ProcessPipes pythonProcess;

struct ParserResult
{
  string title;
  int season; // -1 if not a series
  int episode; // -1 if not an episode
}

ParserResult parseFilename(string fileName) @trusted
{
  if (pythonProcess == ProcessPipes.init)
    pythonProcess = pipeProcess(["python3", "-c", PythonCode], Redirect.all, null, Config.inheritFDs);
  pythonProcess.stdin.writeln(fileName);
  pythonProcess.stdin.flush;
  string output = pythonProcess.stdout.readln.strip;

  Json json = parseJsonString(output);
  ParserResult result;
  result.title = json["title"].get!string;
  result.season = json["season"].opt!int(-1);
  result.episode = json["episode"].opt!int(-1);
  return result;
}
