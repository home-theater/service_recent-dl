module recentdl.rest;

import recentdl.media.media;
import recentdl.media.cache;

import vibe.http.server : HTTPMethod, HTTPServerResponse, render;
import vibe.web.common;

class RestService
{
  @method(HTTPMethod.GET)
  @path("/")
  void index(scope HTTPServerResponse res, bool showFileNames)
  {
    MediaCache.withResource((MediaElement[] mediaElements) {
      auto lastRefresh = MediaCache.getLastRefresh;
      res.render!("index.dt", mediaElements, showFileNames, lastRefresh);
    });
  }

  @method(HTTPMethod.POST)
  @path("/refresh")
  void forceRefresh(scope HTTPServerResponse res)
  {
    MediaCache.forceRefresh;
    res.writeVoidBody;
  }
}
