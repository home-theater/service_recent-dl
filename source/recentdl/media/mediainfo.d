module recentdl.media.mediainfo;

import vibe.core.log;
import vibe.data.json;
import vibe.core.file;
import vibe.http.client;

import std.array : array;
import std.algorithm : map;
import std.exception : enforce;
import std.process : pipeShell, ProcessPipes, wait, Config, Redirect;
import std.string : capitalize, fromStringz, split, strip;

struct MediaInfoResult
{
  string[] audioLangs;
  string[] subLangs;
}

__gshared MediaInfoResultCache resultCache;

shared static this()
{
  resultCache = new MediaInfoResultCache;
}

private class MediaInfoResultCache
{

  enum fileName = "mediaInfoCache.json";

  MediaInfoResult[string] results;

  this()
  {
    if (!existsFile("cache"))
      createDirectory("cache");
    if (existsFile("cache/" ~ fileName))
    {
      string json = cast(string) readFile("cache/" ~ fileName);
      try
        results = deserializeJson!(MediaInfoResult[string])(json);
      catch (Exception e) // Can happen if the request fiber was stopped while writing
        removeFile("cache/" ~ fileName);
    }
  }

  void save()
  {
    synchronized (typeof(this).classinfo)
    {
      if (!existsFile("cache"))
        createDirectory("cache");
      Json json = serializeToJson!(MediaInfoResult[string])(results);
      writeFile("cache/" ~ fileName, cast(ubyte[]) json.toString);
    }
  }

  bool contains(string url)
  {
    synchronized (typeof(this).classinfo)
    {
      return (url in results) !is null;
    }
  }

  MediaInfoResult get(string url)
  {
    synchronized (typeof(this).classinfo)
    {
      enforce(contains(url));
      return results[url];
    }
  }

  void put(string url, MediaInfoResult result)
  {
    synchronized (typeof(this).classinfo)
    {
      results[url] = result;
    }
  }
}

MediaInfoResult mediaInfo(string url, long size) @trusted
{
  if (resultCache.contains(url))
    return resultCache.get(url);

  MediaInfoResult result;

  /*
    Can reduce to single process with
    mediainfo --Output="General;file://template.txt"
    template.txt:
    %Audio_Language_List%
    %Text_Language_List%

    consider increasing to 5MB for mp4
  */
  ProcessPipes procAudio = pipeShell(
    "wget '" ~ url ~ "' -O- | head -c 20000 | mediainfo --Inform=\"General;%Audio_Language_List%\" -", Redirect.all, null, Config.inheritFDs);
  ProcessPipes procSubs = pipeShell(
    "wget '" ~ url ~ "' -O- | head -c 20000 | mediainfo --Inform=\"General;%Text_Language_List%\" -", Redirect.all, null, Config.inheritFDs);

  result.audioLangs = procAudio.stdout.readln.strip.split(" / ").map!capitalize.array;
  result.subLangs = procSubs.stdout.readln.strip.split(" / ").map!capitalize.array;

  wait(procAudio.pid);
  wait(procSubs.pid);

  // logInfo("mediainfo: %s - %s", url, result);

  resultCache.put(url, result);
  resultCache.save();
  return result;
}
