FROM debian:bookworm
ARG ARCH

RUN apt-get update
RUN apt-get install -y unzip curl wget mediainfo python3 python3-pip
RUN pip3 install parse-torrent-title

WORKDIR /app

COPY recent-dl-$ARCH recent-dl
COPY public public

ENV LISTEN_ADDRESS 0.0.0.0
ENV LISTEN_PORT 80
EXPOSE 80

CMD ["./recent-dl"]
