module recentdl.media.cache;

import recentdl.media.media;
import recentdl.media.fetcher;

import vibe.core.log;

import std.datetime : Clock, dur, SysTime;
import std.process : environment;

shared static this()
{
  MediaCache.instance = new MediaCache;
}

class MediaCache
{
  private __gshared MediaCache instance;

  private MediaElement[] mediaElements;
  private SysTime lastRefresh;

  private void refresh()
  {
    logInfo("Refreshing cache");
    instance.lastRefresh = Clock.currTime;
    instance.mediaElements = fetch(environment["API_KEY"]);
  }

  static void forceRefresh()
  {
    synchronized (MediaCache.classinfo)
      instance.refresh;
  }

  static void withResource(void delegate(MediaElement[]) dg)
  {
    synchronized (MediaCache.classinfo)
    {
      if (Clock.currTime > instance.lastRefresh + 15.dur!"minutes")
        instance.refresh;
      dg(instance.mediaElements);
    }
  }

  static SysTime getLastRefresh()
  {
    return instance.lastRefresh;
  }
}
