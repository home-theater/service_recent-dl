- import recentdl.media.media;

- import std.algorithm : all, canFind, filter;
- import std.array : array;
- import std.datetime : SysTime, Clock, Interval, days;
- import std.format : format;

- int columns = showFileNames ? 9 : 8;

- void tableHeader()
  tr
    - if (showFileNames)
      th File name
    th Title
    th Download
    th Copy link
    th Size
    th English audio
    th English subs
    th Dutch subs
    th Date

- bool[] langChecks(MediaElement el)
  - return [
  -   el.audioLangs.canFind("English"),
  -   el.subLangs.canFind("English"),
  -   el.subLangs.canFind("Dutch")
  - ];

- SysTime stripFracSec(SysTime time)
  - return SysTime.fromUnixTime(time.toUnixTime);

- string fileSizeString(long size)
  - auto units = ["bytes", "kB", "MB", "GB"];
  - foreach(unit; units)
  - {
  -   if (size < 1024 || unit == "GB")
  -     return format!"%d %s"(size, unit);
  -   size = size / 1024;
  - }
  - assert(false);

doctype html
html(lang="en")
  head
    title="recent-dl"
    link(rel="stylesheet", href="style.css")
  body
    h1 recent-dl

    // Info section
    h2 Information

    p
      |Use this page to download or stream recently opened media.<br>
      |Mainly intended to download movies/series for offline viewing.

    p
      |All movies and single episodes opened in <strong>the last 3 days</strong> appear here.<br>
      |If you opened an episode that was part of a <strong>season pack</strong>, the entire season will appear here for <strong>about a year</strong>.

    details.infoDetails
      summary Info: Streaming and media players
      p
        |You can also stream every file: Press the "Copy link" button and paste it in a media player.<br>
        |Recommended media players:
        ul
          li VLC (<a href="https://www.videolan.org/vlc/#download">Download</a>):
            ul
              li Desktop/Laptop: Press Ctrl+N, paste the link and press enter to play the copied link.
              li Mobile: In the "More" section, press "New stream", paste the link and press the send button.
          li Celluloid (<a href="https://celluloid-player.github.io/installation.html">Download</a>):
            ul
              li Linux: Press Ctrl+L and press Enter to play the copied link.
          li mpv.net (<a href="https://github.com/stax76/mpv.net/blob/master/docs/Manual.md#download">Download</a>):
            ul
              li Windows: Press Shift+V to play the copied link.
    
    // Download section
    h2 Downloads

    // Refresh
    p
      |Last refreshed on #{stripFracSec(lastRefresh)} (#{stripFracSec(Clock.currTime) - stripFracSec(lastRefresh)} ago)
      button(onclick="fetch('refresh', {method: 'POST'}).then(() => location.reload())", style="margin-left: 8px;") Refresh now

    // Show/Hide filenames
    - if (showFileNames)
      button(onclick="location.href = '?'") Hide file names
    - else
      button(onclick="location.href = '?showFileNames'") Show file names

    // Tables per time interval
    - auto currTime = Clock.currTime;
    - struct TimeFilter { Interval!SysTime interval; string description; bool open; }
    - TimeFilter[] timeFilters = [
    -   TimeFilter(Interval!SysTime(currTime - 1.days, currTime), "Past 24h", true),
    -   TimeFilter(Interval!SysTime(currTime - 7.days, currTime - 1.days), "Past week", false),
    -   TimeFilter(Interval!SysTime(currTime - 30.days, currTime - 7.days), "Past month", false),
    -   TimeFilter(Interval!SysTime(currTime - 365.days, currTime - 30.days), "Past year", false)
    - ];
    - foreach (timeFilter; timeFilters)
      - MediaElement[] filteredTimeElements = mediaElements.filter!(el => timeFilter.interval.contains(SysTime.fromUnixTime(el.date))).array;
      details(open=timeFilter.open)
        summary #{timeFilter.description} (#{filteredTimeElements.length} items)
        table
          - tableHeader();
          - foreach(el; filteredTimeElements)
            - if (el.files.length == 1) // Entry is a single file => one row
              tr
                - MediaFile file = el.files[0];

                // Filename
                - if (showFileNames)
                  td #{file.fileName}

                // Title
                td #{el.title}

                // Download & copy
                td
                  a(href=file.url) Download
                td.copyButtonParent
                  button(onclick="navigator.clipboard.writeText(encodeURI(\"" ~ file.url ~ "\"))") Copy

                // File size
                td #{fileSizeString(file.size)}

                // Language
                - bool[] _langChecks = langChecks(el);
                - foreach (bool langCheck; _langChecks)
                  - if (_langChecks.all!"!a")
                    td.langUnknown Unknown
                  - else if (langCheck)
                    td.langYes Yes
                  - else
                    td.langNo No

                // Date
                td #{SysTime.fromUnixTime(el.date)}
            - else if (el.files.length >= 2) // Entry is a pack of multiple files => subtable
              tr
                // Foldable subtable
                td.subtable(colspan=columns)
                  details
                    summary #{el.title}
                    table
                      - tableHeader();
                      - foreach (file; el.files)
                        tr
                          // Filename
                          - if (showFileNames)
                            td #{file.fileName}

                          // Title
                          - if (file.episode != -1)
                            td #{format!"%sE%d"(el.title, file.episode)}
                          - else
                            td #{el.title}

                          // Download & copy
                          td
                            a(href=file.url) Download
                          td.copyButtonParent
                            button(onclick="navigator.clipboard.writeText(encodeURI(\"" ~ file.url ~ "\"))") Copy

                          // File size
                          td #{fileSizeString(file.size)}

                          // Language
                          - bool[] _langChecks = langChecks(el);
                          - foreach (bool langCheck; _langChecks)
                            - if (_langChecks.all!"!a")
                              td.langUnknown Unknown
                            - else if (langCheck)
                              td.langYes Yes
                            - else
                              td.langNo No

                          // Date
                          td #{SysTime.fromUnixTime(el.date)}
