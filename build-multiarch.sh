#!/usr/bin/env bash

set -e

if [ "$#" -lt 1 ]; then
    echo "Usage: ./script [tag] (optional: extra 'docker buildx build' arguments)"
    exit 1
fi

tag=$1
shift
extra_args=$@

function build {
    arch=$1
    fullarch=$2

    echo ========== Building for arch $arch $fullarch ==========

    if [ -e recent-dl ]; then
        rm recent-dl
    fi

    if [ -e recent-dl-$arch ]; then
        rm recent-dl-$arch
    fi

    dub build #--build=release

    mv recent-dl recent-dl-$arch
    
    docker buildx inspect --bootstrap
    docker buildx build \
        --platform $fullarch \
        --build-arg ARCH=$arch \
        -t hometheater/service_recent-dl:$tag-$arch \
        . $extra_args

    rm recent-dl-$arch
}

function build_cross {
    arch=$1
    fullarch=$2

    echo ========== Building for arch $arch $fullarch ==========

    if [ -e recent-dl ]; then
        rm recent-dl
    fi

    if [ -e recent-dl-$arch ]; then
        rm recent-dl-$arch
    fi

    docker run --rm \
      -v "$(pwd)":/src \
      -v "$HOME/.dub":/root/.dub \
      reavershark/denv:ldc2-cross-$arch-debian-bookworm \
      dub build #--build=release

    mv recent-dl recent-dl-$arch
    
    docker buildx inspect --bootstrap
    docker buildx build \
        --platform $fullarch \
        --build-arg ARCH=$arch \
        -t hometheater/service_recent-dl:$tag-$arch \
        . $extra_args

    rm recent-dl-$arch
}

function create_multiarch_image {
    amends=()
    for arch in $@; do
        amends+=(--amend)
        amends+=(hometheater/service_recent-dl:$tag-$arch)
    done

    docker manifest create hometheater/service_recent-dl:$tag ${amends[@]} 
    docker manifest create hometheater/service_recent-dl:latest ${amends[@]}

    if [[ $extra_args =~ --push ]]; then
        docker manifest push hometheater/service_recent-dl:$tag
        docker manifest push hometheater/service_recent-dl:latest
    fi
}

build amd64 linux/amd64
build_cross arm32v7 linux/arm/v7
build_cross arm64v8 linux/arm64

create_multiarch_image amd64 arm32v7 arm64v8
